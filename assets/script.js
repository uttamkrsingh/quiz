function createNode(text){
    return document.createTextNode(text);
}

function $(element){
   return document.createElement(element);
}

function addClass(element,classes){
    var i=0, 
    classLength = classes.length;
    for(i; i<classLength;i++){
        element.classList.add(classes[i]);
    }
}

function append(parent,child){
    parent.appendChild(child);
}

function load(){
    var mainBody = document.getElementById("main-container");
    var card = $("div");
    addClass(card,['card','text-center'])

    var cardHead = $("div");
    addClass(cardHead,['card-header','text-success'])
    
    var details = "Come and join to the Deadly Contest";

    var text = createNode(details);

    append(cardHead,text);
    append(card,cardHead);
    append(mainBody,card);

    var cardBody = $('div');
    addClass(cardBody,['card-body'])

    var imgDiv = $('div');
    addClass(imgDiv,['text-center','m-2'])

    // logo area
    var logoImg = $('img') ;
    addClass(logoImg,['rounded','border','border-info']);

    var src = "assets/exam.jpg";
    var style ="width : 200px";
    logoImg.setAttribute('src', src);
    logoImg.setAttribute('style', style);

    append(imgDiv,logoImg);
    append(cardBody,imgDiv);
    append(card,cardBody);

    var cardTitle = $('h3');
    addClass(cardTitle,['card-title','text-success','mt-3'])

    var cardFullTitle = createNode('Welcome to Softcripto Contest');
    append(cardTitle,cardFullTitle);
    append(cardBody,cardFullTitle);

    var testYourSelf = $('p');

    addClass(testYourSelf,['card-text','text-info']);

    var bold = $('b');
    var boldLine = createNode('Test Yourself');
    
    append(bold,boldLine);
    append(testYourSelf,bold);
    append(cardBody,testYourSelf);

    //create button
   
    var start = $('button');
    addClass(start,['btn','btn-info']);
    var buttonText = createNode("Let's Start");
    
    append(start,buttonText);
    append(cardBody,start);

    //footer part

    var footerArea = $('div');
    addClass(footerArea,['card-footer','text-muted']);

    var footerText = createNode('Best of luck');
    append(footerArea,footerText);
    append(card,footerArea);

}

load();

